# Lattitude Stock Prices Quiz

This repo contains the Golang solution for the quiz https://gist.github.com/jonog/54e46b5b1200758d222e3c4cf61baaa6

## Run

Run the binary file directly `./quiz` or if you've go installed you can build and run it `go run main.go`

**Typical run results:**

```shell
With the stock prices [1 2 3 4 5], the best buy is (1) and the best sell is (5) for a max profit of (4)
With the stock prices [5 4 3 2 1], unfortunately, there is no chance for a profit at these prices!
With the stock prices [10 7 5 8 11 9], the best buy is (5) and the best sell is (11) for a max profit of (6)
With the stock prices [15 6 12 13 7 4 1 8], the best buy is (6) and the best sell is (13) for a max profit of (7)
```