package main

import (
	"fmt"
)

func getMaxProfit(stockPrices []int) (int, int, int) {
	var bestBuy, bestSell, buy, sell int

	lenStockPrices := len(stockPrices)
	for i := 0; i < lenStockPrices-1; i++ {

		buy, sell = stockPrices[i], 0
		for x := i + 1; x < lenStockPrices; x++ {
			if stockPrices[x] > buy && stockPrices[x] > sell {
				sell = stockPrices[x]
			}
		}

		if sell-buy > bestSell-bestBuy {
			bestBuy, bestSell = buy, sell
		}

	}

	return bestBuy, bestSell, bestSell - bestBuy
}

func processStockPrices(stockPrices []int) {
	b, s, p := getMaxProfit(stockPrices)

	if b == 0 {
		fmt.Printf("With the stock prices %v, unfortunately, there is no chance for a profit at these prices!\n", stockPrices)
	} else {
		fmt.Printf("With the stock prices %v, the best buy is (%d) and the best sell is (%d) for a max profit of (%d)\n", stockPrices, b, s, p)
	}
}

func main() {
	weeklyResults := [][]int{
		[]int{1, 2, 3, 4, 5},
		[]int{5, 4, 3, 2, 1},
		[]int{10, 7, 5, 8, 11, 9},
		[]int{15, 6, 12, 13, 7, 4, 1, 8},
	}

	for _, stockPrices := range weeklyResults {
		processStockPrices(stockPrices)
	}
}
